#!/usr/bin/python

import argparse,  sys

BUCKET_SIZE = 40

def _exit(message):
    print message
    exit(1)
    
    
def check_files(input):
    
    with open(input, "r") as f:
        
        f_firstLine = f.readline()        
        if f_firstLine[0] != '>':
            print 'First line should be something like ">SRR612149_10002231	len=84	cov=4.57". Instead I got:'
            print f_firstLine[0] 
            _exit('It does not look like fastq format... ')
            


def extract_id(header):
    
    id = header.split('\t')[0]
    num = id.split('_')[1].strip('\n')
    curContig = '>contig_' + num +'\n'
    return curContig
    
    
def trimm(input, output,  threshold):    
    
    check_files(input)
    bucket = ''
    curContig = ''
    curSequence = ''
    with open(input, "r") as f, open(output, "w") as w:
        for fi in f:
            if fi[0] ==  '>':           #new contig 
                #print curSequence,  len(curSequence),  threshold
                if len(curSequence)  >= threshold:
                    print len(curSequence)
                    bucket += curContig
                    bucket += curSequence +'\n'
                curSequence = ''
                curContig = extract_id(fi)
            elif fi[0] in ['A', 'G', 'C', 'T', 'N']:
                curSequence += str(fi).strip(' \n')
            else:
                message = 'Unknown 1st character: ', fi[0], ', in line ' +str(i)
                _exit(message)
            
            if len(bucket) > BUCKET_SIZE and  len(curSequence) <= 0 :
                w.write(bucket)
                bucket = ""
        
        print len(curSequence)
        bucket+=curContig
        bucket+=curSequence+'\n'
        w.write(bucket)
    
    
def main():
    parser = argparse.ArgumentParser(description='coming soon')
    parser.add_argument('-f', '--contigs',  help='fasta containing contigs')
    parser.add_argument('-o', '--output',  help='output file (fasta format)')
    parser.add_argument('-t', '--threshold',  help='remove contigs less than this length',  type=int)
    args = parser.parse_args()
    
    #assert (len(args) >= 3)
    trimm(args.contigs, args.output,  args.threshold)

if __name__ == "__main__":
    main()
