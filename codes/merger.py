#!/usr/bin/python

from itertools import islice, izip
import sys, argparse


def _exit(message):
    print message
    exit(1)
    
    
def check_files(forward, reverse):
    
    with open(forward, "r") as f, open(reverse, "r") as r:
        
        f_firstLine = f.readline()
        r_firstLine = r.readline()
        
        if f_firstLine[0] != '@' or r_firstLine[0] != '@':
            print r_firstLine[0] +' <-- ??'
            _exit('It does not look like fastq format... ')
            
        if f_firstLine.split(' ')[0] != r_firstLine.split(' ')[0]:
            _exit('Reads do not much as pair-end')
          
            
def merge(forward, reverse, output):
    
    check_files(forward, reverse)
            
    f_bucket = []
    r_bucket = []
    with open(forward, "r") as f, open(reverse, "r") as r, open(output, "w") as w:
        
        for fi, ri in izip(f, r):
            f_bucket.append(fi)
            r_bucket.append(ri)
            if len(f_bucket) == 4:
                w.write("".join(f_bucket))
                w.write("".join(r_bucket))
                f_bucket = []
                r_bucket = []
    
def main():
    parser = argparse.ArgumentParser(description='coming soon')
    parser.add_argument('-f', '--forward',  help='forward reads')
    parser.add_argument('-r', '--reverse',  help='reverse reads')
    parser.add_argument('-o', '--output',  help='output file')
    args = parser.parse_args()
    
    #assert (args == 4)
    merge(args.forward, args.reverse, args.output)

if __name__ == "__main__":
    main()
