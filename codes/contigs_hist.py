#!/usr/bin/python
import argparse
import matplotlib.pyplot as plt
import numpy as np

def count_contigs(filename):
    
    lengths = []
    total_len = 0
    cur_len = 0
    with open(filename,  'r') as fin:
        fin.readline()
        for line in fin:
            if line[0] != '>':
                cur_len += len(line.strip('\r\n\t '))
            else:
                lengths.append(cur_len)
                total_len += cur_len
                cur_len = 0
    
    return lengths


def main():

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-f', '--file'   ,  help='contigs fasta file')
    parser.add_argument('-b', '--binlen',  help='bin length',  type=int, default=100)
    args = parser.parse_args()
    
    lengths = count_contigs(args.file)
    print lengths
    
    plt.hist(lengths,  50)
    plt.show()



if __name__ == "__main__":
    main()
