#!/bin/bash

rsync -rvazuP chefarov@cineserver.3p.tuc.gr:programming/rna_seq/data /home/chefarov/programming/rna_seq/data
rsync -rvazuP /home/chefarov/programming/rna_seq/mynotes chefarov@cineserver.3p.tuc.gr:programming/rna_seq/
rsync -rvazuP /home/chefarov/programming/rna_seq/codes chefarov@cineserver.3p.tuc.gr:programming/rna_seq/

rsync -rvazuP /home/chefarov/programming/rna_seq/data sbarberakis@clu26.softnet.tuc.gr:
rsync -rvazuP /home/chefarov/programming/rna_seq/mynotes sbarberakis@clu26.softnet.tuc.gr:
rsync -rvazuP /home/chefarov/programming/rna_seq/codes sbarberakis@clu26.softnet.tuc.gr:


rsync -rvazuP --exclude 'DJANGO_python*' /home/chefarov/programming/django chefarov@cineserver.3p.tuc.gr:programming/ 
#rsync -rvazuP --exclude 'DJANGO_python*' /home/chefarov/programming/django chefarov@cineserver.3p.tuc.gr:
