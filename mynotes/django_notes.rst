– SQL / DB –
 
for example, if you have an app named weblog containing a model named Entry, 
then the file sql/entry.sql inside the app’s directory can be used to modify
or insert data into the entries table as soon as it’s been created. 
